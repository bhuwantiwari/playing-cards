
// Playing Cards
// Ethan Corrigan
#include <iostream>
#include <conio.h>
#include <iomanip>
using namespace std;

enum Suit {
	HEARTS,
	SPADES,
	DIAMOND,
	CLUBS
};

enum Rank {
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	ACE
};

struct Card {
	Suit Suit;
	Rank Rank;
};
void PrintCard(Card card);
Card HighCard(Card card1, Card card2);
int main() {
	Card c1;
	c1.Rank = Ten;
	c1.Suit = DIAMOND;


	Card c2;
	c2.Rank = ACE;
	PrintCard(c1);


	(void)_getch();
	return 0;
}

void PrintCard(Card card) {

	switch (card.Rank) {
	case 14: cout << "The Ace of "; break;
	case 13: cout << "The King of "; break;
	case 12: cout << "The Queen of "; break;
	case 11: cout << "The Jack of "; break;
	case 10: cout << "The Ten of "; break;
	case 9: cout << "The Nine of"; break;
	case 8: cout << "The Eight of "; break;
	case 7: cout << "The Seven of"; break;
	case 6: cout << "The Six of "; break;
	case 5: cout << "The Five of "; break;
	case 4: cout << "The Four of "; break;
	case 3: cout << "The Three of "; break;
	case 2: cout << "The Two of "; break;
	default: "Something went wrong";
	}
	switch (card.Suit)
	{
	case 0: cout << "Hearts \n"; break;
	case 1: cout << "Spades \n"; break;
	case 2: cout << "Diamonds \n"; break;
	case 3: cout << "Clubs \n"; break;
	default: "Something went wrong";

	}

}

Card HighCard(Card card1, Card card2) {

	if (card1.Rank > card2.Rank) {

		return card1;
	}
	else {
		return card2;
	}

}